package com.smarthome.dao;

import java.util.List;

import com.smarthome.dao.exception.DBSystemException;
import com.smarthome.model.User;

public interface UserDao {

    List<User> getAll() throws DBSystemException;

    User getBy(int id) throws DBSystemException;

    User getUserByEmail(String email) throws DBSystemException;

    void saveUser(User user) throws DBSystemException;
}

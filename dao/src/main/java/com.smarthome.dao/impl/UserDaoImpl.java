package com.smarthome.dao.impl;

import com.smarthome.dao.UserDao;
import com.smarthome.dao.exception.DBSystemException;
import com.smarthome.dao.util.JdbcDBConnUtils;
import com.smarthome.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao {

	public List<User> getAll() throws DBSystemException {
		String query = "";
		Connection connection = null;
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		List<User> result = null;

		query = "SELECT id, name, password, age from user";

		JdbcDBConnUtils connectUtils = new JdbcDBConnUtils();

		connectUtils.initConn();

		connection = connectUtils.getConnection();

		try {
			stmt = connection.prepareStatement(query);
			resultSet = stmt.executeQuery();
			result = new ArrayList<User>();
			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				String password = resultSet.getString("password");
				int age = resultSet.getInt("age");
				User user = new User();
				user.setId(id);
				user.setName(name);
				user.setPassword(password);
				user.setAge(age);
				result.add(user);
			}
		} catch (SQLException e) {
			throw new DBSystemException("Can't execute SQL = '" + query + "'", e);
		} finally {
			connectUtils.closeQuietly(stmt);
			connectUtils.closeQuietly(connection);
		}
		connectUtils.finalizeConn();
		return result;
	}

	public User getBy(int id) throws DBSystemException{
    		String query = "";
    		Connection connection = null;
    		PreparedStatement stmt = null;
    		ResultSet resultSet = null;
    		User user = null;
    		
    		query = "SELECT id, name, age, password, from user";

    		JdbcDBConnUtils connectUtils = new JdbcDBConnUtils();

    		connectUtils.initConn();

    		connection = connectUtils.getConnection();

    		try {
    			stmt = connection.prepareStatement(query);
    			resultSet = stmt.executeQuery();
    			user = new User();
    			while (resultSet.next()) {
    				user.setId(resultSet.getInt("id"));
					user.setName(resultSet.getString("name"));
					user.setAge(resultSet.getInt("age"));
					user.setPassword(resultSet.getString("password"));
    			}
    		} catch (SQLException e) {
    			throw new DBSystemException("Can't execute SQL = '" + query + "'", e);
    		} finally {
    			connectUtils.closeQuietly(stmt);
    			connectUtils.closeQuietly(connection);
    		}
    		connectUtils.finalizeConn();
    		return user;
	}

    public User getUserByEmail(String email) throws DBSystemException {
        return null;
    }

    public void saveUser(User user) throws DBSystemException {

    }
}

package com.smarthome.dao.util;

import org.apache.log4j.Logger;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.*;
import java.util.Properties;

public class JdbcDBConnUtils {
    private Properties properties = new Properties();
    private Connection connection = null;


    public Connection getConnection() {
        return connection;
    }

    private static final Logger logger = Logger.getLogger(JdbcDBConnUtils.class);

    public void initConn(){
        if(connection == null){
            /*
            URI dbUri = null;
            try {
                Class.forName("org.postgresql.Driver");

                dbUri = new URI(System.getenv("DATABASE_URL"));
                String username = dbUri.getUserInfo().split(":")[0];
                String password = dbUri.getUserInfo().split(":")[1];
                String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath();

                connection =  DriverManager.getConnection(dbUrl, username, password);
            } catch (URISyntaxException e) {
                logger.error("Can't load jdbc driver class", e);
            } catch (ClassNotFoundException e) {
                logger.error("Can't found class", e);
            } catch (SQLException e) {
                logger.error("Can't load jdbc driver class", e);
            }
            */
            try {
                Class.forName("org.postgresql.Driver");

                connection = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/social_network?user=postgres&password=root");

            }catch (ClassNotFoundException e) {
                logger.error("Can't found class", e);
            }catch (SQLException e) {
                System.out.println("Can't load jdbc driver class");
                logger.error("Can't load jdbc driver class", e);
            }
        }
    }

    public void finalizeConn(){
        try {
            connection.close();
        } catch (SQLException e) {
            logger.error("Connection closed fail", e);
        }
    }

    public void rollbackQuietlyConn(Connection connection){
        if(connection != null){
            try {
                connection.rollback();
            } catch (SQLException e) {
                logger.error("Connection rollback fail", e);
                //NOP
            }
        }
    }

    public void closeQuietly(Connection conn){
        if(conn != null){
            try {
                conn.close();
            } catch (SQLException e){
                logger.error("Connection close fail", e);
                //NOP
            }
        }
    }

    public void closeQuietly(PreparedStatement ps){
        if(ps != null){
            try {
                ps.close();
            } catch (SQLException e){
                logger.error("Prepared statement close fail", e);
                //NOP
            }
        }
    }

    public void closeQuietly(ResultSet rs){
        if(rs != null){
            try {
                rs.close();
            } catch (SQLException e){
                logger.error("Result set close fail", e);
                //NOP
            }
        }
    }
}

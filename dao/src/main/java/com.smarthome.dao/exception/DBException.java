package com.smarthome.dao.exception;

class DBException extends Exception {

    DBException(String message) {
        super(message);
    }

    DBException(String message, Throwable cause) {
        super(message, cause);
    }
}

package com.smarthome.controller;

import com.smarthome.dao.exception.DBSystemException;
import com.smarthome.service.UserService;
import com.smarthome.service.impl.UserServiceImplMock;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name="usercontr",urlPatterns={"/users"})
public class UserController extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {

        UserService userService = new UserServiceImplMock();

        String message = "Hello UserController method doGet";

        PrintWriter out = response.getWriter();
        try {
            out.println("<h1>" + message + "</h1>" + "</br>" +
                    "<p>" + userService.getUserById(1).getName() + "</p>");
                            } catch (DBSystemException e) {
                                e.printStackTrace();
                            }

    }
}

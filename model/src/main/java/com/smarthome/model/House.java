package com.smarthome.model;

public class House {

    private int id;

    private int square;


    public enum Type {MAIN, OTHER}

    @Override
    public String toString() {
        return "House{" +
                "id=" + id +
                ", square=" + square +
                '}';
    }
}

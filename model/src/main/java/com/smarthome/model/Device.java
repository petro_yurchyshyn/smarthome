package com.smarthome.model;

public class Device {

    private int id;

    public enum Type {TV, FRIG, AUDIO, LIGHT, CLIMATECONTROL}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Device{" +
                "id=" + id +
                '}';
    }
}

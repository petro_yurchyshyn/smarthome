package com.smarthome.service.impl;

import com.smarthome.dao.UserDao;
import com.smarthome.dao.exception.DBSystemException;
import com.smarthome.dao.impl.UserDaoImpl;
import com.smarthome.model.User;
import com.smarthome.service.UserService;

import java.util.ArrayList;
import java.util.List;

public class UserServiceImplMock implements UserService{


    public User getUserByEmail(String email) throws DBSystemException {
        return null;
    }

    public void saveUser(User user) throws DBSystemException {
    }

    public List<User> getAll() throws DBSystemException {
        return getUsersMock();
    }

    public User getUserById(int id) throws DBSystemException {
        return getUserMock();
    }

    private User getUserMock(){
        User user = new User();
        user.setId(1);
        user.setName("Alex");
        user.setAge(22);
        user.setPassword("1234");
        return user;
    }

    private List<User> getUsersMock(){
        List<User> users = new ArrayList<User>();
        users.add(getUserMock());
        users.add(getUserMock());
        users.add(getUserMock());
        return null;
    }

}

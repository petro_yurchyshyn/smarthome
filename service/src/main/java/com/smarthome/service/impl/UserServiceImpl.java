package com.smarthome.service.impl;

import com.smarthome.dao.UserDao;
import com.smarthome.dao.exception.DBSystemException;
import com.smarthome.dao.impl.UserDaoImpl;
import com.smarthome.model.User;
import com.smarthome.service.UserService;

import java.util.List;

public class UserServiceImpl implements UserService{

    UserDao userDao = new UserDaoImpl();

    public User getUserByEmail(String email) throws DBSystemException {
        return userDao.getUserByEmail(email);
    }

    public void saveUser(User user) throws DBSystemException {
        userDao.saveUser(user);
    }

    public List<User> getAll() throws DBSystemException {
        return userDao.getAll();
    }

    public User getUserById(int id) throws DBSystemException {
        return userDao.getBy(id);
    }
}

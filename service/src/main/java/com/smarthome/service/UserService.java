package com.smarthome.service;

import com.smarthome.dao.exception.DBSystemException;
import com.smarthome.model.User;

import java.util.List;


public interface UserService {

    User getUserByEmail(String email) throws DBSystemException;

    void saveUser(User user) throws DBSystemException;

    List<User> getAll() throws DBSystemException;

    User getUserById(int id) throws DBSystemException;
}
